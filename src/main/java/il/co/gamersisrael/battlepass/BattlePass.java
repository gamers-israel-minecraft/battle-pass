package il.co.gamersisrael.battlepass;

import il.co.gamersisrael.battlepass.commands.OpenGUICommand;
import il.co.gamersisrael.battlepass.events.ConditionsEvents;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public final class BattlePass extends JavaPlugin {

    private static Economy econ = null;

    public static BattlePass instance;
    @Override
    public void onEnable() {
        instance = this;
        if (!setupEconomy() ) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        Bukkit.getPluginManager().registerEvents(new ConditionsEvents(), this);

        getCommand("battlepass").setExecutor(new OpenGUICommand());


        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                ConditionsEvents.timer();
            }
        },0,20);
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }


    public static Economy getEconomy() {
        return econ;
    }


    @Override
    public void onDisable() {
    }
}
