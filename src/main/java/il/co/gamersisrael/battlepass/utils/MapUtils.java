package il.co.gamersisrael.battlepass.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MapUtils {

    public static String playerMapToValue(Map<UUID,Integer> map){
        StringBuilder r = new StringBuilder();

        for (UUID p : map.keySet()) {
            r.append(",").append(p.toString()).append(":").append(map.get(p));
        }

        return r.toString().substring(1);
    }

    public static Map<UUID,Integer>  playerMapToKey(String map){
        Map<UUID,Integer> r = new HashMap<>();


        for (String kv : map.split(",")) {
            r.put(UUID.fromString(kv.split(":")[0]),Integer.parseInt(kv.split(":")[0]));
        }

        return r;
    }
}
