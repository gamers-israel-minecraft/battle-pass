package il.co.gamersisrael.battlepass.commands;

import il.co.gamersisrael.battlepass.quest.Quest;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.UUID;

public class OpenGUICommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            ((Player) sender).openInventory(getGui(((Player) sender).getUniqueId()));
        }
        return false;
    }

    public static Inventory getGui(UUID player){
        Inventory inv = Bukkit.createInventory(null,27, "Quests");
        List<Quest> quests = Quest.loadAll();
        int questsToPut = 5;
        for (Quest q : quests) {
            if(!q.players.contains(player)){
                inv.setItem(16-questsToPut, q.toItemStack(player));
                questsToPut--;
            }
            if (questsToPut == 0){
                break;
            }
        }
        return inv;
    }

}
