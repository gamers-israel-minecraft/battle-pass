package il.co.gamersisrael.battlepass.quest;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

public abstract class Reward implements ConfigurationSerializable {

    public abstract void give(Player player);
}
