package il.co.gamersisrael.battlepass.quest;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class XP extends Reward {

    int amount;

    public XP(Map<String, Object> map){
        amount = (int) map.get("xp");
    }

    public XP(int amount){
        this.amount = amount;
    }
    @Override
    public void give(Player player) {
        player.giveExp(amount);
    }

    @Override
    public Map<String, Object> serialize() {Map<String,Object> r = new HashMap<>();

        r.put("xp",amount);

        return r;
    }

    @Override
    public String toString() {
        return "Get " + amount + " XP";
    }
}
