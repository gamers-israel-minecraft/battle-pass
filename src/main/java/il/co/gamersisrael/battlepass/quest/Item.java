package il.co.gamersisrael.battlepass.quest;

import il.co.gamersisrael.battlepass.utils.MapUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class Item extends Reward {

    ItemStack is;

    public Item(Map<String, Object> map){
        is = (ItemStack) map.get("item");
    }

    public Item(ItemStack is){
        this.is = is;
    }
    @Override
    public void give(Player player) {
        player.getInventory().addItem(is);
    }

    @Override
    public Map<String, Object> serialize() {Map<String,Object> r = new HashMap<>();

        r.put("item",is);

        return r;
    }

    @Override
    public String toString() {
        return "Get " + is.getType().toString() + " X " + is.getAmount();
    }
}
