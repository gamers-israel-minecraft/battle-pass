package il.co.gamersisrael.battlepass.quest;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import il.co.gamersisrael.battlepass.utils.MapUtils;
import org.bukkit.Material;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class IsLevel extends Condition {

    public int amount;

    public Map<UUID,Integer> val = new HashMap<>();

    public IsLevel(Map<String, Object> map){
        amount = (int) map.get("amount");
        val = MapUtils.playerMapToKey((String)map.get("values"));
    }

    public IsLevel(Material type, int amount){
        this.amount = amount;
    }

    @Override
    public boolean completed(UUID player) {
        return ASkyBlockAPI.getInstance().getLongIslandLevel(player) >= amount;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String,Object> r = new HashMap<>();

        r.put("amount",amount);
        r.put("values", MapUtils.playerMapToValue(val));

        return r;
    }

    @Override
    public String toString() {
        return "be  online for " + amount + " seconds";
    }
}
