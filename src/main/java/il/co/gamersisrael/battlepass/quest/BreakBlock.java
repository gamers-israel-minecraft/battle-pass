package il.co.gamersisrael.battlepass.quest;

import il.co.gamersisrael.battlepass.utils.MapUtils;
import org.bukkit.Material;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BreakBlock extends Condition {

    public Material type;
    public int amount;

    public Map<UUID,Integer> val = new HashMap<>();

    public BreakBlock(Map<String, Object> map){
        amount = (int) map.get("amount");
        type =  Material.getMaterial((String)map.get("type"));
        val = MapUtils.playerMapToKey((String)map.get("values"));
    }

    public BreakBlock(Material type, int amount){
        this.amount = amount;
        this.type = type;
    }

    @Override
    public boolean completed(UUID player) {
        return val.containsKey(player) && val.get(player) >= amount;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String,Object> r = new HashMap<>();

        r.put("amount",amount);
        r.put("type",type.toString());
        r.put("values", MapUtils.playerMapToValue(val));

        return r;
    }

    @Override
    public String toString() {
        return "Break " + type.toString() + " " + amount + " times";
    }
}
