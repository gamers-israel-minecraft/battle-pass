package il.co.gamersisrael.battlepass.quest;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import java.util.UUID;

public abstract class Condition implements ConfigurationSerializable {

    public abstract boolean completed(UUID player);
}
