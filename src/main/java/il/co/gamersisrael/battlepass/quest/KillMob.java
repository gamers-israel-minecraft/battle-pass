package il.co.gamersisrael.battlepass.quest;

import il.co.gamersisrael.battlepass.utils.MapUtils;
import org.bukkit.entity.EntityType;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class KillMob extends Condition {

    public EntityType type;
    public int amount;

    public Map<UUID,Integer> val = new HashMap<>();

    public KillMob(Map<String, Object> map){
        amount = (int) map.get("amount");
        type =  EntityType.valueOf((String)map.get("type"));
        val = MapUtils.playerMapToKey((String)map.get("values"));
    }

    public KillMob(EntityType type, int amount){
        this.amount = amount;
        this.type = type;
    }

    @Override
    public boolean completed(UUID player) {
        return val.containsKey(player) && val.get(player) >= amount;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String,Object> r = new HashMap<>();

        r.put("amount",amount);
        r.put("type",type.toString());
        r.put("values", MapUtils.playerMapToValue(val));

        return r;
    }

    @Override
    public String toString() {
        return "kill " + type.toString() + " " + amount + " times";
    }
}
