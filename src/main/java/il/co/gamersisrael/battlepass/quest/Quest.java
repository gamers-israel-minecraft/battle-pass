package il.co.gamersisrael.battlepass.quest;

import com.google.common.io.Files;
import il.co.gamersisrael.battlepass.BattlePass;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Quest {

    List<Condition> conditions = new ArrayList<>();
    List<Reward> rewards = new ArrayList<>();
    String name;

    public List<UUID> players;

    public Quest(String name){
        this.name = name;
    }

    public Quest addReward(Reward reward){
        this.rewards.add(reward);
        return this;
    }

    public List<Condition> getConditions(){
        return conditions;
    }
    public Quest addCondition(Condition condition){
        this.conditions.add(condition);
        return this;
    }

    public Quest setReward(Reward ... rewards){
        this.rewards = Arrays.asList(rewards);
        return this;
    }

    public Quest setReward(List<Reward> rewards){
        this.rewards = rewards;
        return this;
    }

    public Quest setFinished(UUID ... conditions){
        this.players = Arrays.asList(conditions);
        return this;
    }

    public Quest setFinished(List<UUID> conditions){
        this.players = conditions;
        return this;
    }
    public Quest setConditions(Condition ... conditions){
        this.conditions = Arrays.asList(conditions);
        return this;
    }

    public Quest setConditions(List<Condition> conditions){
        this.conditions = conditions;
        return this;
    }
    public ItemStack toItemStack(UUID player){
        ItemStack is = new ItemStack(Material.PAPER);
        ItemMeta im = is.getItemMeta();

        List<String> lore = new ArrayList<>();

        for (Condition c : conditions) {
            ChatColor chatColor = c.completed(player) ? ChatColor.GREEN : ChatColor.RED;
            lore.add(chatColor + c.toString());
        }
        for (Reward r : rewards) {
            lore.add(ChatColor.AQUA + r.toString());
        }

        im.setDisplayName(name);
        im.setLore(lore);

        is.setItemMeta(im);
        return is;
    }
    public void save(){
        File f = new File(new File(BattlePass.instance.getDataFolder(),"quests"), name + ".yaml");
        f.mkdirs();
        if(!f.exists()){{
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }}
        YamlConfiguration qConf = new YamlConfiguration();
        qConf.set("name", name);
        for (int i = 0; i < rewards.size(); i++) {
            qConf.set("rewards." + i , rewards.get(i));
        }
        List<String> finished = new ArrayList<>();
        for (UUID u : players) {
            finished.add(u.toString());
        }
        qConf.set("finished", finished);
        for (int i = 0; i < conditions.size(); i++) {
            qConf.set("conditions." + i , conditions.get(i));
        }
        try {
            qConf.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static Quest load(String name){
        YamlConfiguration qConf = new YamlConfiguration();
        File f = new File(new File(BattlePass.instance.getDataFolder(),"quests"), name + ".yaml");
        try {
            qConf.load(f);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        Set<String> keys = qConf.getConfigurationSection("conditions").getKeys(true);
        List<Condition> conditions = new ArrayList<>();
        for(String key : keys)
        {
            conditions.add((Condition) qConf.get("conditions." + key));
        }

        keys = qConf.getConfigurationSection("rewards").getKeys(true);

        List<Reward> rewards = new ArrayList<>();
        for(String key : keys)
        {
            rewards.add((Reward) qConf.get("rewards." + key));
        }


        List<UUID> finished = new ArrayList<>();

        for (String s : qConf.getStringList("finished")) {
            finished.add(UUID.fromString(s));
        }

        return new Quest(name).setReward(rewards).setConditions(conditions).setFinished(finished);
    }

    public static List<Quest> loadAll(){
        List<Quest> quests = new ArrayList<>();
        for (File f : Objects.requireNonNull(new File(BattlePass.instance.getDataFolder(), "quests").listFiles())) {
            quests.add(load(Files.getNameWithoutExtension(f.getName())));
        }
        return quests;
    }

    public void giveReward(Player player){
        for (Condition condition : conditions) {
            if(!condition.completed(player.getUniqueId())){
                return;
            }
        }
        for (Reward reward : rewards) {
            reward.give(player);
        }
    }
}
