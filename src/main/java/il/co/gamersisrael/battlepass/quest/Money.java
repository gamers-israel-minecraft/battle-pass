package il.co.gamersisrael.battlepass.quest;

import il.co.gamersisrael.battlepass.BattlePass;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class Money extends Reward {

    double amount;

    public Money(Map<String, Object> map){
        amount = (double) map.get("money");
    }

    public Money(double amount){
        this.amount = amount;
    }
    @Override
    public void give(Player player) {
        BattlePass.getEconomy().depositPlayer(player,amount);
    }

    @Override
    public Map<String, Object> serialize() {Map<String,Object> r = new HashMap<>();

        r.put("money",amount);

        return r;
    }

    @Override
    public String toString() {
        return "Get " + amount + " money";
    }
}
