package il.co.gamersisrael.battlepass.events;

import il.co.gamersisrael.battlepass.quest.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.List;

public class ConditionsEvents implements Listener {

    public static void timer(){
        for (Player player : Bukkit.getOnlinePlayers()) {
            List<Quest> quests = Quest.loadAll();
            for(Quest q : quests) {
                for (Condition c : q.getConditions()) {
                    if (c instanceof Time) {
                        Time w = (Time) c;
                        if (w.val.containsKey(player.getUniqueId())){
                            w.val.put(player.getUniqueId(), w.val.get(player.getUniqueId()) + 1);
                        } else {
                            w.val.put(player.getUniqueId(), 0);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        List<Quest> quests = Quest.loadAll();
        for(Quest q : quests){
            for (Condition c : q.getConditions()) {
                if (c instanceof Walk) {
                    Walk w = (Walk) c;
                    if (w.val.containsKey(event.getPlayer().getUniqueId())){
                        w.val.put(event.getPlayer().getUniqueId(), w.val.get(event.getPlayer().getUniqueId()) + (int)event.getFrom().distance(event.getTo()));
                    } else {
                        w.val.put(event.getPlayer().getUniqueId(), 0);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if(event.getEntity().getLastDamageCause() instanceof Player){
            Player player = event.getEntity().getKiller();
            List<Quest> quests = Quest.loadAll();
            for(Quest q : quests){
                for (Condition c : q.getConditions()) {
                    if (c instanceof KillMob){
                        KillMob km = (KillMob) c;
                        if (km.type == event.getEntityType()){
                            if (km.val.containsKey(player.getUniqueId())){
                                km.val.put(player.getUniqueId(), km.val.get(player.getUniqueId()) +1);
                            }else {
                                km.val.put(player.getUniqueId(), 1);
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        List<Quest> quests = Quest.loadAll();
        for(Quest q : quests){
            for (Condition c : q.getConditions()) {
                if(c instanceof BreakBlock){
                    BreakBlock bb = (BreakBlock) c;
                    if(bb.type == event.getBlock().getType()){
                        if (bb.val.containsKey(event.getPlayer().getUniqueId())){
                            bb.val.put(event.getPlayer().getUniqueId(), bb.val.get(event.getPlayer().getUniqueId()) +1);
                        }else {
                            bb.val.put(event.getPlayer().getUniqueId(), 1);
                        }
                    }
                }
            }
        }
    }
}
